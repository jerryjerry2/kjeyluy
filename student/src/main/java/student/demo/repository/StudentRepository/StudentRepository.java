package student.demo.repository.StudentRepository;

import student.demo.repository.model.Student;

import java.util.List;

public interface StudentRepository {
    int getLastId();
    List<Student> viewAll();
    Boolean insert (Student student);
    Boolean delete (int id);


}
