package student.demo.repository;

import org.springframework.stereotype.Repository;
import student.demo.repository.StudentRepository.StudentRepository;
import student.demo.repository.model.Student;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepositoryImp implements StudentRepository {
    List<Student> studentList = new ArrayList<>();

    @Override
    public Boolean delete(int id) {
        for(int i = 0;i<studentList.size();i++){
            if(studentList.get(i).getId() == id){
                studentList.remove(i);
            }
        }
        return true;
    }

    @Override
    public int getLastId() {
        return studentList.size() < 1 ? 1 : studentList.get(studentList.size() - 1).getId() + 1;
    }

    @Override
    public Boolean insert(Student student) {
        studentList.add(student);
        return true;
    }

    @Override
    public List<Student> viewAll() {
        System.out.println("Repo List : "+studentList);
        return studentList;
    }
}
