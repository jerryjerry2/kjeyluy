package student.demo.service.StudentService;

import student.demo.repository.model.Student;

import java.util.List;

public interface StudentService {
    int getLastId();
    List<Student> viewAll();
    Boolean insert(Student student);
    Boolean delete(int id);
}
