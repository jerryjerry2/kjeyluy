package student.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import student.demo.repository.StudentRepository.StudentRepository;
import student.demo.repository.model.Student;
import student.demo.service.StudentService.StudentService;

import java.util.List;

@Service
public class StudentServiceImp implements StudentService {
    @Autowired
    StudentRepository studentRepository;

    @Override
    public Boolean delete(int id) {
        return studentRepository.delete(id);
    }

    @Override
    public Boolean insert(Student student) {
        return studentRepository.insert(student);
    }

    @Override
    public int getLastId() {
        return studentRepository.getLastId();
    }

    @Override
    public List<Student> viewAll() {

        return studentRepository.viewAll();
    }
}
