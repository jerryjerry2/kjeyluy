package student.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import student.demo.repository.model.Student;
import student.demo.service.StudentService.StudentService;

@Controller
public class StudentController {
    @Autowired
    StudentService studentService;

    @GetMapping("/home")
    String test (ModelMap modelMap){
        modelMap.addAttribute("students",studentService.viewAll());
        return "index";
    }

    @GetMapping("/insert")
    String insert (ModelMap modelMap){
        modelMap.addAttribute("getId",studentService.getLastId());
        return "insert";
    }

    @PostMapping("insert")
    String insertStudent(@ModelAttribute Student student,ModelMap modelMap){
        modelMap.addAttribute("getId",studentService.getLastId());

        student.setId(studentService.getLastId());
        studentService.insert(student);
        return "redirect:/home";
    }

    @GetMapping("/delete/{id}")
    String delete(@PathVariable int id){
        System.out.println(id);

        return "redirect:/home";
    }
}
